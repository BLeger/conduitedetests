import org.mockserver.mock.action.ExpectationCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import static org.mockserver.model.HttpResponse.notFoundResponse;
import static org.mockserver.model.HttpResponse.response;


public class ExpectationCallbackHandler implements ExpectationCallback {

    public HttpResponse handle(HttpRequest httpRequest) {
        if (httpRequest.getPath().getValue().endsWith("/signin")) {
            return response()
                .withStatusCode(200)
                .withHeader("Content-Type", "'application/json'")
                .withBody("{'username': 'mockTestBand','authorities': [{'authority': 'ROLE_BAND'}],'accessToken': 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtb2NrVGVzdEJhbmQiLCJpYXQiOjE2MTA5NjkzMzksImV4cCI6MTYxMDk3MjkzOX0.LEKZMjiYT_eOwbeJuavyRbkU0bf8uIPzWM4l00Qt9ddbYSaPwqjoJ_CjYIvJazQV9OTW8a47jK_b5C0gNB4GRA','tokenType': 'Bearer'}");
        } else {
            return notFoundResponse();
        }
    }

}
