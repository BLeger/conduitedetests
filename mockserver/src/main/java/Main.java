import org.mockserver.client.MockServerClient;

import static org.mockserver.model.HttpClassCallback.callback;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class Main {

    public static void main(String[] args){
        createExpectationLoginWithCallBack();
        createExpectationGetFestivals();
        createExpectationGetConcerts();
    }

    private static void createExpectationLoginWithCallBack(){
        new MockServerClient("127.0.0.1", 1080)
            .when(
                request()
                    .withMethod("POST")
                    .withPath("/auth/signin")
                    .withBody("{'username': 'bandTest', 'password': 'passwordTest'}")
            )
            .respond(
                callback()
                    .withCallbackClass("ExpectationCallbackHandler")
            )
        ;
    }

    private static void createExpectationGetFestivals(){
        new MockServerClient("127.0.0.1", 1080)
            .when(
                request()
                    .withMethod("GET")
                    .withPath("/festivals")
            )
            .respond(
                response()
                    .withStatusCode(200)
                    .withHeader("Content-Type", "'application/json'")
                    .withBody("{'id':1,'festivalName':'TestFestival','description':'Ceci est un festival test','festAdmin':{'id':7,'username':'orgatest','password':'$2a$10$zxTlxSuyGtiNZork/uA/JOLx0ASwuNn.eq0K72HOry2y0UwcbTJii','roles':[{'id':3,'name':'ROLE_ORG'}]}},{'id':2,'festivalName':'FestivalName1','description':'Description1','festAdmin':{'id':10,'username':'organiser_test','password':'$2a$10$JYfJDayYFIcCWPOUni9FDOzMZaDjDgdtOS3lKi7LEY3o7ikQ6gIY2','roles':[{'id':3,'name':'ROLE_ORG'}]}},{'id':3,'festivalName':'covidparty','description':'warsasate','festAdmin':{'id':13,'username':'yoyo','password':'$2a$10$0S3N8eL8AChydhce9mZGWORw5JZcoNwlNcfBLdTfc1B5bYjBsZJ2u','roles':[{'id':3,'name':'ROLE_ORG'}]}},{'id':6,'festivalName':'j adorelestestsangular','description':'c est faux','festAdmin':{'id':17,'username':'benoitorg','password':'$2a$10$XcByeXegJbLGxrrRys7bOO1DjB.8lD7BZnkpi8RnqxIpHqrJu/rvO','roles':[{'id':3,'name':'ROLE_ORG'}]}}")
            )
        ;
    }

    private static void createExpectationGetConcerts(){
        new MockServerClient("127.0.0.1", 1080)
            .when(
                request()
                    .withMethod("GET")
                    .withPath("/bands/bandTest/concerts")
            )
            .respond(
                response()
                    .withStatusCode(200)
                    .withHeader("Content-Type", "'application/json'")
                    .withBody("[{'id':2,'start':'05-01-2021 15:30','duration':'15:32','band':{'id':9,'username':'band_test','password':'$2a$10$DcDS/kXZEgQOqi54imYkluE8.BWfCM01tVsQ/Me0iyBM47jbbfR0e','roles':[{'id':4,'name':'ROLE_BAND'}],'name':'band_test','musicType':'band_test music type','description':null},'stage':{'id':2,'name':'ezr','festival':{'id':2,'festivalName':'FestivalName1','description':'Description1','festAdmin':{'id':10,'username':'organiser_test','password':'$2a$10$JYfJDayYFIcCWPOUni9FDOzMZaDjDgdtOS3lKi7LEY3o7ikQ6gIY2','roles':[{'id':3,'name':'ROLE_ORG'}]}}}},{'id':3,'start':'08-01-2021 09:51','duration':'23:59','band':{'id':9,'username':'band_test','password':'$2a$10$DcDS/kXZEgQOqi54imYkluE8.BWfCM01tVsQ/Me0iyBM47jbbfR0e','roles':[{'id':4,'name':'ROLE_BAND'}],'name':'band_test','musicType':'band_test music type','description':null},'stage':{'id':3,'name':'covid19','festival':{'id':3,'festivalName':'covidparty','description':'warsasate','festAdmin':{'id':13,'username':'yoyo','password':'$2a$10$0S3N8eL8AChydhce9mZGWORw5JZcoNwlNcfBLdTfc1B5bYjBsZJ2u','roles':[{'id':3,'name':'ROLE_ORG'}]}}}}]")
            )
        ;
    }
}
