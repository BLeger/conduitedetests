import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/internal/Observable';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SpecFestivalsComponent } from './spec-festivals.component';
import { Festival } from '../../models/festival.model';
import { FestivalService } from '../../services/festival.service';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';

describe('SpecFestivalsComponent', () => {
  let component: SpecFestivalsComponent;
  let fixture: ComponentFixture<SpecFestivalsComponent>;

  // Mock du FestivalService
  let festivalServiceStub: Partial<FestivalService>;
  festivalServiceStub = {
    getFestivals(): Observable<Festival[]> {
      let festivals: Festival[] = [new Festival("nom", "description"), new Festival("second", "festival")];
      
      return new Observable(observer => {
        observer.next(festivals);
        observer.complete();
      });
    }
  };
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatTableModule],
      declarations: [ SpecFestivalsComponent ],
      providers: [ // Utilisation du mock
        { provide: FestivalService, useValue: festivalServiceStub } 
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecFestivalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should receive all festivals', () => {
    expect(component.festList.length).toBe(2);
  });

  it('should display all festivals', () => {
    fixture.detectChanges();

    let rows = fixture.nativeElement.querySelectorAll('tbody tr');
    expect(rows.length).toBe(2);

    let firstRow = rows[0];
    expect(firstRow.cells[0].innerHTML).toContain("nom");
    expect(firstRow.cells[1].innerHTML).toContain("description");

    let secondRow = rows[1];
    expect(secondRow.cells[0].innerHTML).toContain("second");
    expect(secondRow.cells[1].innerHTML).toContain("festival");
  });
});
