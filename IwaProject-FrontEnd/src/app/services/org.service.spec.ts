import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { OrganiserService } from './org.service';

describe('OrgService', () => {
  let service: OrganiserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(OrganiserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
