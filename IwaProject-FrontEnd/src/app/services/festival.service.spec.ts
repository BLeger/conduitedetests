import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FestivalService } from './festival.service';

describe('FestivalService', () => {
  let service: FestivalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(FestivalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
