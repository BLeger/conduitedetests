import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { SpecService } from './spec.service';

describe('SpecService', () => {
  let service: SpecService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(SpecService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
