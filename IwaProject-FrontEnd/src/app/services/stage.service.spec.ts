import { TestBed } from '@angular/core/testing';

import { HttpClientModule} from '@angular/common/http';
import { StageService } from './stage.service';

describe('StageService', () => {
  let service: StageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(StageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
