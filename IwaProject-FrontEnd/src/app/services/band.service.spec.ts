import { TestBed } from '@angular/core/testing';

import { HttpClientModule} from '@angular/common/http';
import { BandService } from './band.service';

describe('BandService', () => {
  let service: BandService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(BandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
