import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { ConcertService } from './concert.service';

describe('ConcertService', () => {
  let service: ConcertService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(ConcertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
