
describe('Scénério : utilisateur de type Band', () => {
  it('Visits the homepage', () => {
    //go to the main page of the app
    cy.visit('/')
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
  })

  it('Création de l utilisateur', () => {
    cy.get('.signup-spec').click()
    cy.url().should('eq', Cypress.config().baseUrl + '/signup') // => true

    cy.contains('Band').click()
    cy.get('#mat-input-0').click().type('userTest').should('have.value','userTest')
    cy.get('#mat-input-1').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('#mat-input-2').click().type('bandName').should('have.value','bandName')
    cy.get('#mat-input-3').click().type('musicType').should('have.value','musicType')
    cy.get('#mat-input-4').click().type('description').should('have.value','description')

    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Your registration is successful. Please login!')
  })


  it('Tests non passant connexion compte', () => {
    cy.get('[ng-reflect-router-link="/login"]').click()
    cy.url().should('eq', Cypress.config().baseUrl +'/login') // => true

    cy.get('#mat-input-5').click().type('testest').should('have.value','testest')
    cy.get('#mat-input-6').click().type('testest').should('have.value','testest')

    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Error -> Unauthorized')
    cy.url().should('eq', Cypress.config().baseUrl +'/login') // => true
  })
})

describe('Scénério : utilisateur de type Organiser', () => {

  it('Visits the homepage', () => {
    //go to the main page of the app
    cy.visit('/')
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
  })

  it('Création de l utilisateur', () => {
    cy.get('.signup-spec').click()
    cy.contains('Organiser').click()
    cy.get('#mat-input-0').click().type('organisateurTest').should('have.value','organisateurTest')
    cy.get('#mat-input-1').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Your registration is successful. Please login!')
  })

  it('Connexion de l utilisateur ', () => {
    cy.get('[ng-reflect-router-link="/login"]').click()
    cy.get('#mat-input-2').click().type('organisateurTest').should('have.value','organisateurTest')
    cy.get('#mat-input-3').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
  })

  it('Page principal Organisateur ', () => {
    cy.url().should('not.be.eq', Cypress.config().baseUrl + '/signup')
    cy.contains('Logged in as ROLE_ORG')
  })

  it('Ajout festival ', () => {
    cy.wait(500)
    cy.contains("My Festivals").click()
    cy.url().should('eq', Cypress.config().baseUrl + '/orgFest')
    cy.contains("Name")
    cy.contains("Description")
    cy.get('app-org-fest.ng-star-inserted > .mat-focus-indicator').dblclick()
    cy.get('#mat-input-1').click().type('festivalTest').should('have.value','festivalTest')
    cy.get('#mat-input-2').click().type('descriptionTest').should('have.value','descriptionTest')
    cy.get('.mat-dialog-actions > .mat-accent').click()
  })

  it('Vérifier les informations ', () => {
    cy.get('.mat-row > .cdk-column-Name').should('have.text',' festivalTest ')
    cy.get('.mat-row > .cdk-column-Description').should('have.text',' descriptionTest ')
    cy.get('.cdk-column-Action > .mat-accent')
    cy.get('.bntAction2')
  })

  it('Ajout concert ', () => {
    cy.get('.mat-row > .cdk-column-Name').dblclick()
    cy.url().should('include', Cypress.config().baseUrl + '/orgStage')

    cy.get('.addButton').click()
    cy.get('#mat-dialog-title-1').should('have.text','Stage registration')
    cy.get('#mat-input-4').click().type('stageTest').should('have.value','stageTest')
    cy.get('.mat-dialog-actions > .mat-accent').click()
  })

  it('Vérifier les informations ', () => {
    cy.get('.mat-row > .cdk-column-Name').should('have.text',' stageTest ')
    cy.get('.cdk-column-Action > .mat-accent').should('exist')
    cy.get('.bntAction2').should('exist')
  })

  it('Ajout groupe ', () => {
    cy.get('.mat-row > .cdk-column-Name').dblclick()
    cy.url().should('include', Cypress.config().baseUrl + '/orgConcert')

    cy.get('.addButton').click()

    cy.get('#mat-input-6').click().type('01/01/2021').should('have.value','01/01/2021') //choose a date
    cy.get('#mat-input-7').click().type('10:00').should('have.value','10:00') //choose an hour

    cy.get('.mat-select-placeholder').click()
    cy.get('.mat-option-text').should('have.text',' bandName ').click()
    cy.get('#mat-input-8').click().type('01:00').should('have.value','01:00') //choose a duration
    cy.get('.mat-dialog-actions > .mat-accent').click()
  })

  it('Vérifier les informations ', () => {
    cy.get('.mat-row > .cdk-column-bandName').should('have.text',' bandName ')
    cy.get('.mat-row > .cdk-column-startTime').should('have.text',' 01-01-2021 10:00 ')
    cy.get('.mat-row > .cdk-column-duration').should('have.text',' 01:00 ')
    cy.get('.cdk-column-Action > .mat-accent').should('exist')
    cy.get('.bntAction2').should('exist')
  })

  it('Retour homepage', () => {
    cy.get('[aria-label="home icon"] > .mat-button-wrapper > .mat-icon').click()
    cy.url().should('eq', Cypress.config().baseUrl +'/home')
    cy.get('app-home > h1').should('have.text','Welcome to the IwaEvent website !!!')

  })
    it('Se déconnecter ', () => {
      cy.get('.logout').dblclick()
      cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
      cy.contains('Welcome to the IwaEvent website !!!')

    })
})


describe('Scénario : utilisateur de type Band 2', () => {

  it('Visits the homepage', () => {
    //go to the main page of the app
    cy.visit('/')
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true

  })

  it('Se connecter', () => {
    cy.get('[ng-reflect-router-link="/login"]').click()
    cy.get('#mat-input-0').click().type('userTest').should('have.value','userTest')
    cy.get('#mat-input-1').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Logged in as ROLE_BAND')
    cy.url().should('not.be.eq', Cypress.config().baseUrl + '/signup')

  })

  it('Vérifier inscription concert', () => {
    cy.get('[ng-reflect-router-link="/bandConcert"]').click()
    cy.url().should('eq', Cypress.config().baseUrl +'/bandConcert') // => true

    cy.get('.mat-row > .cdk-column-Festival').should('have.text',' festivalTest ')
    cy.get('.mat-row > .cdk-column-Date').should('have.text',' 01-01-2021 10:00 ')
    cy.get('.mat-row > .cdk-column-Duration').should('have.text',' 01:00 ')
  })

  it('Déconnexion', () => {
    cy.get('.logout > .mat-button-wrapper > .mat-icon').dblclick()
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
    cy.contains('Welcome to the IwaEvent website !!!')
  })

})







describe('Scénario : utilisateur de type Spectator', () => {
  it('Visits the homepage', () => {
    //go to the main page of the app
    cy.visit('/')
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
  })
  it('Création de l utilisateur', () => {

    cy.get('.signup-spec').click()
    cy.contains('Spectator').click()

    cy.get('#mat-input-0').click().type('userTest2').should('have.value','userTest2')
    cy.get('#mat-input-1').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('#mat-input-2').click().type('firstName').should('have.value','firstName')
    cy.get('#mat-input-3').click().type('lastName').should('have.value','lastName')

    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Your registration is successful. Please login!')
  })



  it('Se connecter', () => {
    cy.reload()
    cy.get('[ng-reflect-router-link="/login"]').click()

    cy.get('#mat-input-0').click().type('userTest2').should('have.value','userTest2')
    cy.get('#mat-input-1').click().type('passwordTest').should('have.value','passwordTest')
    cy.get('.ng-untouched > .mat-focus-indicator').should('not.be.disabled')
    cy.get('.ng-untouched > .mat-focus-indicator').click()
    cy.contains('Logged in as ROLE_SPEC')
    cy.url().should('not.be.eq', Cypress.config().baseUrl + '/signup')

  })

  it('Commander un festival', () => {
    cy.get('[ng-reflect-router-link="/specFest"]').click()
    cy.url().should('eq', Cypress.config().baseUrl +'/specFest') // => true

    cy.get('.mat-row > .cdk-column-Name').should('have.text',' festivalTest ')
    cy.get('.mat-row > .cdk-column-Descritpion').should('have.text',' descriptionTest ')
    cy.get('.cdk-column-Order > .mat-focus-indicator').click()
  })

  it('Verifier que le festival est bien enregistré', () => {
    cy.get('[ng-reflect-router-link="/specOwnFest"]').click()
    cy.url().should('eq', Cypress.config().baseUrl +'/specOwnFest') // => true

    cy.get('.mat-row > .cdk-column-Name').should('have.text',' festivalTest ')
    cy.get('.mat-row > .cdk-column-Description').should('have.text',' descriptionTest ')
    cy.get('.cdk-column-Cancel > .mat-focus-indicator').should('exist')
  })


  it('Déconnexion', () => {
    cy.get('.logout > .mat-button-wrapper > .mat-icon').dblclick()
    cy.url().should('eq', Cypress.config().baseUrl +'/home') // => true
    cy.contains('Welcome to the IwaEvent website !!!')
  })

})

