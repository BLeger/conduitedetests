describe('Voici le premier scénario, aller dans tous les liens', () => {
  it('Visits the homepage', () => {
    //go to the main page of the app
    cy.visit('/')
    cy.url().should('eq', 'http://localhost:4200/home') // => true
  })
  it('Visits the login page', () => {
    //go to the login page of the app
    cy.get('[ng-reflect-router-link="/login"]').click()
    cy.url().should('eq', 'http://localhost:4200/login') // => true

    cy.get('#mat-input-0').should('be.empty') //par défaut le Username est vide
    cy.get('#mat-input-1').should('be.empty') //par défaut le Password est vide
    cy.contains('Username') //regarde si la page contiens le mot Username
    cy.contains('Password') //regarde si la page contiens le mot Password
    cy.get('#mat-hint-0').should('have.text','At least 6 characters')

    cy.get('p > .mat-focus-indicator > .mat-button-wrapper').should('have.text', "Sign Up")

    cy.get('.ng-untouched > .mat-focus-indicator').should('be.disabled')

  })
  it('Visits the signup page', () => {
    //go to the signup page of the app
    cy.get('.signup-spec').click()
    cy.url().should('eq', 'http://localhost:4200/signup') // => true
    cy.get('#mat-button-toggle-1-button > .mat-button-toggle-label-content').should('have.text','Spectator')
    cy.get('#mat-button-toggle-2-button > .mat-button-toggle-label-content').should('have.text','Band')
    cy.get('#mat-button-toggle-3-button > .mat-button-toggle-label-content').should('have.text','Organiser')

    /*Check the Spectator panel */
    cy.contains('Spectator').click()
    cy.get('#mat-input-2').should('be.empty') //par défaut le Username est vide
    cy.get('#mat-input-3').should('be.empty') //par défaut le Password est vide
    cy.get('#mat-input-4').should('be.empty') //par défaut le FirstName est vide
    cy.get('#mat-input-5').should('be.empty') //par défaut le LastName est vide
    cy.contains('Username') //regarde si la page contiens le mot Username
    cy.contains('Password') //regarde si la page contiens le mot Password
    cy.contains('First Name') //regarde si la page contiens le mot Username
    cy.contains('Last Name') //regarde si la page contiens le mot Password
    cy.get('.ng-untouched > .mat-focus-indicator').should('be.disabled')


    /*Check the Band panel */
    cy.contains('Band').click()

    cy.get('#mat-input-6').should('be.empty') //par défaut le Username est vide
    cy.get('#mat-input-7').should('be.empty') //par défaut le Password est vide
    cy.get('#mat-input-8').should('be.empty') //par défaut le BandName est vide
    cy.get('#mat-input-9').should('be.empty') //par défaut le MusicType est vide
    cy.get('#mat-input-10').should('be.empty') //par défaut le Description est vide

    cy.contains('Username') //regarde si la page contiens le mot Username
    cy.contains('Password') //regarde si la page contiens le mot Password
    cy.contains('Band Name') //regarde si la page contiens le mot Band Name
    cy.contains('Music Type') //regarde si la page contiens le mot Music Type
    cy.contains('Description') //regarde si la page contiens le mot Description

    cy.get('.ng-untouched > .mat-focus-indicator').should('be.disabled')


    /*Check the Organiser panel */
    cy.contains('Organiser').click()
    cy.get('#mat-input-11').should('be.empty') //par défaut le Username est vide
    cy.get('#mat-input-12').should('be.empty') //par défaut le Password est vide

    cy.contains('Username') //regarde si la page contiens le mot Username
    cy.contains('Password') //regarde si la page contiens le mot Password

    cy.get('.ng-untouched > .mat-focus-indicator').should('be.disabled')

  })




  })
