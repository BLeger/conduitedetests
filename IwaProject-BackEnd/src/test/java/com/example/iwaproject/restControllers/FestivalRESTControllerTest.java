package com.example.iwaproject.restControllers;

import com.example.iwaproject.model.FestAdmin;
import com.example.iwaproject.model.Festival;
import com.example.iwaproject.repositories.FestivalRepository;
import com.example.iwaproject.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class FestivalRESTControllerTest {

    @Autowired
    private FestivalRESTController festivalRESTController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FestivalRepository festivalRepository;

    private static final String FESTIVAL_1_NAME = "name1";
    private static final String FESTIVAL_2_NAME = "name2";
    private static final String FESTIVAL_1_DESCRIPTION = "description1";
    private static final String FESTIVAL_2_DESCRIPTION = "description2";

    private FestAdmin admin;

    private Festival festival1;
    private Festival festival2;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws InstantiationException {
        userRepository.deleteAll();
        FestAdmin admin = new FestAdmin("username", "password");
        userRepository.save(admin);

        festival1 = new Festival(FESTIVAL_1_NAME, FESTIVAL_1_DESCRIPTION, admin);
        festival1.setStages(new ArrayList<>());
        festivalRepository.save(festival1);

        festival2 = new Festival(FESTIVAL_2_NAME, FESTIVAL_2_DESCRIPTION, admin);
        festivalRepository.save(festival2);

        mockMvc = MockMvcBuilders.standaloneSetup(festivalRESTController)
                .addPlaceholderValue("crossorigin.url","http://localhost:4200").build();
    }

    @Test
    public void findAllFestivalsTest() throws Exception {
        this.mockMvc.perform(
                get("/festivals")
                .accept(MediaType.APPLICATION_JSON))
                //.andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))

                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(festival1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].festivalName").value(FESTIVAL_1_NAME))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(FESTIVAL_1_DESCRIPTION))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].festAdmin").isNotEmpty())

                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(festival2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].festivalName").value(FESTIVAL_2_NAME))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].description").value(FESTIVAL_2_DESCRIPTION))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].festAdmin").isNotEmpty());
    }

    @Test
    public void findFestivalsTest() throws Exception {
        this.mockMvc.perform(
                get("/festivals/" + festival1.getId().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(festival1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.festivalName").value(FESTIVAL_1_NAME))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(FESTIVAL_1_DESCRIPTION))
                .andExpect(MockMvcResultMatchers.jsonPath("$.festAdmin").isNotEmpty());
    }

    @Test
    public void deleteFestivalsTest() throws Exception {
        // Valid id
        this.mockMvc.perform(
                delete("/festivals/" + festival1.getId().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());

        // Invalid id
        this.mockMvc.perform(
                delete("/festivals/456748768788")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
