package com.example.iwaproject.repositories;

import com.example.iwaproject.model.FestAdmin;
import com.example.iwaproject.model.Festival;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FestivalRepositoryTest {

    private static final String FESTIVAL_NAME = "name";
    private static final String FESTIVAL_NAME_UPDATE = "name2";
    private static final String FESTIVAL_DESCRIPTION = "description";
    private static final String FESTIVAL_DESCRIPTION_UPDATE = "description2";

    private FestAdmin admin;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FestivalRepository festivalRepository;

    @BeforeEach
    public void setup() throws InstantiationException {
        userRepository.deleteAll();
        FestAdmin admin = new FestAdmin("username", "password");
        userRepository.save(admin);
    }

    @Test
    public void addFestivalTest() {
        Festival festival = new Festival(FESTIVAL_NAME, FESTIVAL_DESCRIPTION, admin);
        festivalRepository.save(festival);

        Festival newFestival = festivalRepository.findById(festival.getId()).get();

        assertEquals(FESTIVAL_NAME, newFestival.getFestivalName());
        assertEquals(FESTIVAL_DESCRIPTION, newFestival.getDescription());
    }

    @Test
    public void updateFestivalTest() {
        Festival festival = new Festival(FESTIVAL_NAME, FESTIVAL_DESCRIPTION, admin);
        festivalRepository.save(festival);

        festival.setFestivalName(FESTIVAL_NAME_UPDATE);
        festival.setDescription(FESTIVAL_DESCRIPTION_UPDATE);
        festivalRepository.save(festival);

        Festival newFestival = festivalRepository.findById(festival.getId()).get();

        assertEquals(FESTIVAL_NAME_UPDATE, newFestival.getFestivalName());
        assertEquals(FESTIVAL_DESCRIPTION_UPDATE, newFestival.getDescription());
    }

    @Test
    public void deleteFestivalTest() {
        Festival festival = new Festival(FESTIVAL_NAME, FESTIVAL_DESCRIPTION, admin);
        festivalRepository.save(festival);

        festivalRepository.delete(festival);

        assertEquals(0, festivalRepository.count());
    }

    @Test
    public void deleteByIdFestivalTest() {
        Festival festival = new Festival(FESTIVAL_NAME, FESTIVAL_DESCRIPTION, admin);
        festivalRepository.save(festival);

        festivalRepository.deleteById(festival.getId());

        assertEquals(0, festivalRepository.count());
    }
}
